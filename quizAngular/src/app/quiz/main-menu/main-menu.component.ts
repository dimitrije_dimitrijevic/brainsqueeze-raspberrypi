import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ServerCommunicationService} from "../../services/server-communication.service";
import {NgbProgressbarConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
  providers: [NgbProgressbarConfig]
})
export class MainMenuComponent implements OnInit {

  showAlertMessage = false;


  constructor(public router: Router) {

  }

  ngOnInit() {

  }


}
