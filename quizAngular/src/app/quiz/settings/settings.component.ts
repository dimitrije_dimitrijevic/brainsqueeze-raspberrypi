import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ServerCommunicationService} from "../../services/server-communication.service";
import {Settings} from "../../model/Settings";
import {Router} from "@angular/router";
import {NgbProgressbarConfig} from "@ng-bootstrap/ng-bootstrap";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  formSettings: FormGroup;
  progressHTML: HTMLElement;
  progressValue = 0;
  intervalProgress: any;
  imgLogo: string = 'http://localhost:443/images/main-logo.png';
  changeLogo: boolean = true;
  categories: Array<{ category: string, id: number }> = [];

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private config: NgbProgressbarConfig,
              private serverCommunication: ServerCommunicationService) {
    this.formSettings = this.formBuilder.group({
      timeOneQuestion: ['', Validators.required],
      numberQuestion: ['', Validators.required],
      firstCorrectAnswer: ['', Validators.required],
      secondCorrectAnswer: ['', Validators.required],
      thirdCorrectAnswer: ['', Validators.required],
      fourthCorrectAnswer: ['', Validators.required],
      wrongAnswer: ['', Validators.required],
      category: [''],
      logoImgUrl: ['http://quiz.itcentar.rs/logo-big.png']
    });

    config.max = 100;
    config.striped = true;
    config.animated = true;
    config.type = 'warning';
    config.height = '60px';
  }

  ngOnInit() {
    this.initForm();
    this.progressHTML = document.querySelector("div.settings-progress");
  }

  getCheckedCategory(category: string): boolean {
    return this.formSettings.controls['category'].value.includes(category);
  }

  saveSettings(): void {
    let categoriesCheckBox: any = document.querySelectorAll('input.categories-quiz[type="checkbox"]:checked');
    let categoriesNew: string[] = [];
    if (categoriesCheckBox) {
      for (let i = 0; i < categoriesCheckBox.length; i++) {
        categoriesNew.push(categoriesCheckBox[i].value)
      }
    }
    this.formSettings.controls['category'].setValue(categoriesNew);
    let settings: Settings = this.formSettings.value;
    this.serverCommunication.postSetSettings(settings).subscribe((res) => {
      // console.log(res);
      if (res.success) {
        this.router.navigate(['']);
      } else {
        this.activeAlert('Došlo je do greške, podešavanja nisu upamćena, pokušajte kasnije.');
      }
    }, (err) => {
      this.activeAlert('Nedostupan server.');
    });
  }

  activeAlert(message: string): void {
    let div: HTMLElement = document.createElement("div");
    div.className = "custom-alert bounceInDown animated";
    div.innerHTML = message;
    document.body.appendChild(div);
    console.log(div);

    setTimeout(() => {
      div.remove();
    }, 5000);
  }

  syncQuestion(): void {
    clearInterval(this.intervalProgress);
    this.progressHTML.classList.add('active');
    this.progressValue = 0;
    this.downloadQuestionProgress(10);
    let questionsSubscription: Subscription = this.serverCommunication.syncQuestion().subscribe((data: any) => {
      this.downloadQuestionProgress(130);
      questionsSubscription.unsubscribe();
    }, (err) => {
      clearInterval(this.intervalProgress);
      questionsSubscription.unsubscribe();
      this.progressHTML.classList.remove('active');
      this.activeAlert('Došlo je do greške, pokušajte ponovo sinhronizujete pitanja.');
    });
  }

  downloadQuestionProgress(number) {
    this.intervalProgress = setInterval(() => {
      this.progressValue++;
      if (this.progressValue >= 110) {
        this.progressHTML.classList.remove('active');
        this.initForm();
      }
      if (this.progressValue == number) {
        clearInterval(this.intervalProgress);
      }
    }, 80);
  }

  downloadLogo(): void {
    if (this.formSettings.controls['logoImgUrl'].value.length > 0) {
      this.imgLogo = '';
      this.changeLogo = false;
      let downloadSubscription: Subscription = this.serverCommunication.postDownloadLogo(this.formSettings.controls['logoImgUrl'].value).subscribe((data: any) => {
        this.changeLogo = true;
        this.imgLogo = 'http://localhost:443/images/main-logo.png';
        downloadSubscription.unsubscribe();
      }, (err) => {
        downloadSubscription.unsubscribe();
        this.activeAlert('Došlo je do greške, pokušajte ponovo da preuzmete logo.');
      });
    }
  }

  initForm(): void {
    this.serverCommunication.getSettings().subscribe((req: any) => {
      if (req.success) {
        this.categories = [];
        let currSettings: Settings = req.settings;
        this.formSettings.controls['timeOneQuestion'].setValue(currSettings.timeOneQuestion);
        this.formSettings.controls['numberQuestion'].setValue(currSettings.numberQuestion);
        this.formSettings.controls['firstCorrectAnswer'].setValue(currSettings.firstCorrectAnswer);
        this.formSettings.controls['secondCorrectAnswer'].setValue(currSettings.secondCorrectAnswer);
        this.formSettings.controls['thirdCorrectAnswer'].setValue(currSettings.thirdCorrectAnswer);
        this.formSettings.controls['fourthCorrectAnswer'].setValue(currSettings.fourthCorrectAnswer);
        this.formSettings.controls['wrongAnswer'].setValue(currSettings.wrongAnswer);
        this.formSettings.controls['category'].setValue(currSettings.category);
        this.formSettings.controls['logoImgUrl'].setValue(currSettings.logoImgUrl);
        currSettings.categories.map((item, index) => {
          this.categories.push({category: item, id: index});
        });
      }
    }, (err) => {
      this.activeAlert('Nedostupan server.');
    });
  }

  backPage() {
    window.history.back();
  }
}
