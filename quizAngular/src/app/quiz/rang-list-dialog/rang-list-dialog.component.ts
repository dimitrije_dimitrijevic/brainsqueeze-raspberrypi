import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";
import {MainQuizComponent} from "../main-quiz/main-quiz.component";
import {Gamer} from "../../model/Gamer";
import {ServerCommunicationService} from "../../services/server-communication.service";
import {Router} from "@angular/router";
import {ForwardDataService} from "../../services/forward-data.service";

@Component({
  selector: 'app-rang-list-dialog',
  templateUrl: './rang-list-dialog.component.html',
  styleUrls: ['./rang-list-dialog.component.scss']
})
export class RangListDialogComponent implements OnInit {
  scoreTeam: Array<Gamer>;

  constructor(private router: Router,
              private dialog: MatDialog,
              private forwardData: ForwardDataService,
              private dialogRef: MatDialogRef<MainQuizComponent>,
              @Inject(MAT_DIALOG_DATA) private data: Array<Gamer>,
              private serverCommunication: ServerCommunicationService) {
    let pom;
    let sorted = false;
    if (data != null) {
      while (!sorted) {
        sorted = true;
        for (let i = 0; i < data.length - 1; i++) {
          if (data[i].score < data[i + 1].score) {
            pom = data[i];
            data[i] = data[i + 1];
            data[i + 1] = pom;
            sorted = false;
          }
        }
      }
      this.scoreTeam = data;
    }
  }

  ngOnInit() {
  }

  onNoClick(): void {
    // this.dialogRef.close();
  }

  saveScore(): void {
    this.serverCommunication.postScoreLastGame(this.getGamer()).subscribe((res) => {
      // this.router.navigate(["/start"]);
      this.dialogRef.close(false);
    });
  }

  playAgain(): void {
    this.serverCommunication.postScoreLastGame(this.getGamer()).subscribe(() => {
      let subscriptionGetQuestion = this.serverCommunication.getGameQuestion().subscribe((res) => {
        if (res.success) {
          this.forwardData.currentGame = res.game;
          this.dialogRef.close(true);
        } else {
          subscriptionGetQuestion.unsubscribe();
        }
      }, (err) => {
        subscriptionGetQuestion.unsubscribe();
      });
    });
  }

  getGamer(): any {
    let score: any = [];
    this.scoreTeam.filter((item) => {
      score.push({
        teamId: item.teamId,
        name: item.name,
        score: item.score
      })
    });
    return score;
  }
}
