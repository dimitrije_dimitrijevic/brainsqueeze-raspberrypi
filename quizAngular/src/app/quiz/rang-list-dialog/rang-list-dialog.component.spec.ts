import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RangListDialogComponent } from './rang-list-dialog.component';

describe('RangListDialogComponent', () => {
  let component: RangListDialogComponent;
  let fixture: ComponentFixture<RangListDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RangListDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RangListDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
