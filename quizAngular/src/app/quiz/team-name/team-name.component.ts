import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Gamer} from '../../model/Gamer';
import {ForwardDataService} from '../../services/forward-data.service';
import {ServerCommunicationService} from "../../services/server-communication.service";
import {Subscription} from "rxjs/Subscription";
import {Constants} from '../../constants';

@Component({
  selector: 'app-team-name',
  templateUrl: './team-name.component.html',
  styleUrls: ['./team-name.component.scss']
})
export class TeamNameComponent implements OnInit {

  formTeamName: FormGroup;
  newGroupTeam: Array<Gamer>;
  subscriptionGetQuestion: Subscription;

  constructor(public router: Router,
              private formBuilder: FormBuilder,
              private forwardData: ForwardDataService,
              private serverCommunication: ServerCommunicationService) {
    this.formTeamName = formBuilder.group({
      team1: ['', [Validators.required,]],
      team2: ['', [Validators.required,]],
      team3: ['', [Validators.required,]],
      team4: ['', [Validators.required,]]
    });
  }

  ngOnInit() {
    this.newGroupTeam = [];
  }

  saveTeam(): void {
    this.newGroupTeam = [];
    let position = 0;
    for (const field in this.formTeamName.controls) {
      const control = this.formTeamName.get(field);
      this.newGroupTeam.push(new Gamer(position, control.value));
      position++;
    }
    if (this.newGroupTeam.length == 4) {
      this.subscriptionGetQuestion = this.serverCommunication.getGameQuestion().subscribe((res) => {
        if (res.success) {
          this.forwardData.groupTeam = this.newGroupTeam;
          this.forwardData.currentGame = res.game;
          this.forwardData.startNewGame = Constants.randomS;
          this.router.navigate(['main-quiz/' + Constants.randomS]);
        } else {
          this.subscriptionGetQuestion.unsubscribe();
          this.activeAlert('Došlo je do greške, pokušajte ponovo');
        }
      }, (err) => {
        this.subscriptionGetQuestion.unsubscribe();
        this.activeAlert('Došlo je do greške, pokušajte ponovo');
      });
    }
  }

  activeAlert(message: string): void {
    let div: HTMLElement = document.createElement("div");
    div.className = "custom-alert bounceInDown animated";
    div.innerHTML = message;
    // div.setAttributeNodeNS();
    document.body.appendChild(div);
    console.log(div);

    setTimeout(() => {
      div.remove();
    }, 5000);
  }

  backPage(){
    window.history.back();
  }
}
