import {Component, OnInit, OnDestroy, Inject, AfterViewInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Game} from '../../model/Game';
import {OnPressTaster} from '../../interfaces/OnPressTaster';
import {Gamer} from '../../model/Gamer';
import {Taster} from '../../model/Taster';
import {Question} from '../../model/Question';
import {ChatServiceService} from '../../services/chat-service.service';
import {ForwardDataService} from '../../services/forward-data.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";
import {RangListDialogComponent} from "../rang-list-dialog/rang-list-dialog.component";
import {HostListener} from '@angular/core';
import {PlatformLocation} from "@angular/common";
// import {setInterval, setTimeout, clearInterval, clearTimeout} from 'timers';

@Component({
  selector: 'app-main-quiz',
  templateUrl: './main-quiz.component.html',
  styleUrls: ['./main-quiz.component.scss']
})
export class MainQuizComponent implements OnInit, OnDestroy, OnPressTaster, AfterViewInit {
  closeResult: string;

  timerQuestionElement: HTMLElement;
  timerGameInterval: any;
  timerQuestionTimeout: any;
  timerQuestionInterval: any;
  timerGame: string;
  timerGameSec: number;
  timerGameMin: number;
  timerQuestion: number;
  totalNumberQuestion: number;
  currentNumberQuestion: number;
  endQuestion = false;
  currentGame: Game;
  currentGroupTeam: Array<Gamer> = [];
  currentQuestion: Question;
  checkInPosition: number = 1;
  checkInPositionVisibility: Array<number> = [0, 0, 0, 0];

  nextTasterStep: number = 0;
  joystickReset: Taster = new Taster(false, false, false, false);
  joystickTeam_1: Taster = this.joystickReset;
  joystickTeam_2: Taster = this.joystickReset;
  joystickTeam_3: Taster = this.joystickReset;
  joystickTeam_4: Taster = this.joystickReset;

  private timerHTMLElement: any;
  tasterTeam1Active = false;
  private joystick1CircleA: HTMLElement;
  private joystick1CircleB: HTMLElement;
  private joystick1CircleC: HTMLElement;
  private joystick1CircleD: HTMLElement;

  tasterTeam2Active = false;
  private joystick2CircleA: HTMLElement;
  private joystick2CircleB: HTMLElement;
  private joystick2CircleC: HTMLElement;
  private joystick2CircleD: HTMLElement;

  tasterTeam3Active = false;
  private joystick3CircleA: HTMLElement;
  private joystick3CircleB: HTMLElement;
  private joystick3CircleC: HTMLElement;
  private joystick3CircleD: HTMLElement;

  tasterTeam4Active = false;
  private joystick4CircleA: HTMLElement;
  private joystick4CircleB: HTMLElement;
  private joystick4CircleC: HTMLElement;
  private joystick4CircleD: HTMLElement;

  private audioTick = new Audio();
  private audioSound = new Audio();
  private audioTaster1 = new Audio();
  private audioTaster2 = new Audio();
  private audioTaster3 = new Audio();
  private audioTaster4 = new Audio();

  occupiedPoint: Array<boolean> = [false, false, false, false];
  sortedCalledFunctions: Array<{ position: number, team: number }> = [];

  constructor(private router: Router,
              private activeRoute: ActivatedRoute,
              private chatService: ChatServiceService,
              private forwardData: ForwardDataService,
              public dialog: MatDialog,
              private platformLocation: PlatformLocation) {
    this.platformLocation.onPopState(() => {
      console.log('pressed back!');
      this.ngOnDestroy();
    });
    if (this.forwardData.startNewGame == activeRoute.snapshot.params.newGame) {
      this.chatService.onPressTaster = this;
      this.currentGroupTeam = this.forwardData.groupTeam.slice();
      this.currentGame = this.forwardData.currentGame;
      this.currentQuestion = new Question('', ['', '', '', ''], 0, '');
      this.totalNumberQuestion = this.currentGame.question.length;
      this.currentNumberQuestion = 0;
    } else {
      router.navigate(['/error']);
    }
    this.chatService.messages.subscribe(msg => {
      // console.log(msg);
    });
  }

  ngAfterViewInit() {

  }

  ngOnInit() {
    this.selectElementOnHTML();
    this.resetTasterIndicator();
    let delayStartGame = setTimeout(() => {
      if (this.currentGame != null) {
        this.currentQuestion = this.currentGame.question[this.currentNumberQuestion];
        this.startQuestionTimer();
      }
      clearTimeout(delayStartGame);
    }, 500);
  }

  ngOnDestroy(): void {
    document.body.style.overflow = 'unset';
    this.clearIntervals();
  }

  startQuestionTimer() {
    this.timerQuestion = this.currentGame.settings.timeOneQuestion;
    this.playAudioSound();
    this.timerQuestionInterval = setInterval(() => {
      // console.log(new Date());
      this.timerQuestion--;
      // if (this.timerQuestion == 7) {
      //   this.chatService.sendMsg('RADI MITO');
      // }
      if (this.timerQuestion == 4) {
        this.timerHTMLElement.querySelector('img.answers-timer-alert').style.visibility = 'visible';
        this.timerHTMLElement.querySelector('img.answers-timer-alert').classList.add('blink');
        this.stopAudioSound();
        this.playAudioTick();
      }
      if (this.timerQuestion == 0) {
        this.endQuestion = true;
        this.timerHTMLElement.querySelector('img.answers-timer-alert').classList.remove('blink');
        this.timerHTMLElement.querySelector('img.answers-timer-alert').style.visibility = 'hidden';
        clearInterval(this.timerQuestionInterval);
        this.stopAudioTick();
      }
    }, 1000);
  }

  clearIntervals(): void {
    clearInterval(this.timerGameInterval);
    clearInterval(this.timerQuestionInterval);
    // clearTimeout(this.timerQuestionTimeout);
    this.stopAudioSound();
    this.stopAudioTick();
    this.audioTaster1.pause();
    this.audioTaster1.currentTime = 0;
    this.audioTaster2.pause();
    this.audioTaster2.currentTime = 0;
    this.audioTaster3.pause();
    this.audioTaster3.currentTime = 0;
    this.audioTaster4.pause();
    this.audioTaster4.currentTime = 0;
  }

  pressTeam1(taster: Taster): void {
    if (this.timerQuestion > 0) {
      // if (!this.tasterTeam1Active && this.timerQuestion <= 10) {
      if (!this.tasterTeam1Active) {
        this.audioTaster1.play();
        this.joystickTeam_1 = taster;
        this.tasterTeam1Active = true;
        this.checkInPositionVisibility[0] = this.checkInPosition;
        this.sortedCalledFunctions.push({position: this.checkInPosition, team: 1});
        this.checkInPosition++;

      }
    }
  }

  pressTeam2(taster: Taster): void {
    if (this.timerQuestion > 0) {
      // if (!this.tasterTeam2Active && this.timerQuestion <= 10) {
      if (!this.tasterTeam2Active) {
        this.audioTaster2.play();
        this.joystickTeam_2 = taster;
        this.tasterTeam2Active = true;
        this.checkInPositionVisibility[1] = this.checkInPosition;
        this.sortedCalledFunctions.push({position: this.checkInPosition, team: 2});
        this.checkInPosition++;
      }
    }
  }

  pressTeam3(taster: Taster): void {
    if (this.timerQuestion > 0) {
      // if (!this.tasterTeam3Active && this.timerQuestion <= 10) {
      if (!this.tasterTeam3Active) {
        this.audioTaster3.play();
        this.joystickTeam_3 = taster;
        this.tasterTeam3Active = true;
        this.checkInPositionVisibility[2] = this.checkInPosition;
        this.sortedCalledFunctions.push({position: this.checkInPosition, team: 3});
        this.checkInPosition++;
      }
    }
  }

  pressTeam4(taster: Taster): void {
    if (this.timerQuestion > 0) {
      // if (!this.tasterTeam4Active && this.timerQuestion <= 10) {
      if (!this.tasterTeam4Active) {
        this.audioTaster4.play();
        this.joystickTeam_4 = taster;
        this.tasterTeam4Active = true;
        this.checkInPositionVisibility[3] = this.checkInPosition;
        this.sortedCalledFunctions.push({position: this.checkInPosition, team: 4});
        this.checkInPosition++;
      }
    }
  }

  team1TasterShowing(taster: Taster) {
    if (taster.answerA) {
      this.joystick1CircleA.querySelector('img').src = './assets/image/traffic-light-green-active.png';
      let point = this.getMyPoint(0, this.checkInPositionVisibility[0], this.tasterTeam1Active);
      this.joystick1CircleA.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[0].score += point;
    } else {
      this.joystick1CircleA.querySelector('img').src = './assets/image/traffic-light-green.png';
      this.joystick1CircleA.querySelector('span').innerHTML = 'A';
    }

    if (taster.answerB) {
      this.joystick1CircleB.querySelector('img').src = './assets/image/traffic-light-red-active.png';
      let point = this.getMyPoint(1, this.checkInPositionVisibility[0], this.tasterTeam1Active);
      this.joystick1CircleB.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[0].score += point;
    } else {
      this.joystick1CircleB.querySelector('img').src = './assets/image/traffic-light-red.png';
      this.joystick1CircleB.querySelector('span').innerHTML = 'B';
    }

    if (taster.answerC) {
      this.joystick1CircleC.querySelector('img').src = './assets/image/traffic-light-yellow-active.png';
      let point = this.getMyPoint(2, this.checkInPositionVisibility[0], this.tasterTeam1Active);
      this.joystick1CircleC.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[0].score += point;
    } else {
      this.joystick1CircleC.querySelector('img').src = './assets/image/traffic-light-yellow.png';
      this.joystick1CircleC.querySelector('span').innerHTML = 'C';
    }

    if (taster.answerD) {
      this.joystick1CircleD.querySelector('img').src = './assets/image/traffic-light-blue-active.png';
      let point = this.getMyPoint(3, this.checkInPositionVisibility[0], this.tasterTeam1Active);
      this.joystick1CircleD.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[0].score += point;
    } else {
      this.joystick1CircleD.querySelector('img').src = './assets/image/traffic-light-blue.png';
      this.joystick1CircleD.querySelector('span').innerHTML = 'D';
    }
  }

  team2TasterShowing(taster: Taster) {
    if (taster.answerA) {
      this.joystick2CircleA.querySelector('img').src = './assets/image/traffic-light-green-active.png';
      let point = this.getMyPoint(0, this.checkInPositionVisibility[1], this.tasterTeam2Active);
      this.joystick2CircleA.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[1].score += point;
    } else {
      this.joystick2CircleA.querySelector('img').src = './assets/image/traffic-light-green.png';
      this.joystick2CircleA.querySelector('span').innerHTML = 'A';
    }

    if (taster.answerB) {
      this.joystick2CircleB.querySelector('img').src = './assets/image/traffic-light-red-active.png';
      let point = this.getMyPoint(1, this.checkInPositionVisibility[1], this.tasterTeam2Active);
      this.joystick2CircleB.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[1].score += point;
    } else {
      this.joystick2CircleB.querySelector('img').src = './assets/image/traffic-light-red.png';
      this.joystick2CircleB.querySelector('span').innerHTML = 'B';
    }

    if (taster.answerC) {
      this.joystick2CircleC.querySelector('img').src = './assets/image/traffic-light-yellow-active.png';
      let point = this.getMyPoint(2, this.checkInPositionVisibility[1], this.tasterTeam2Active);
      this.joystick2CircleC.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[1].score += point;
    } else {
      this.joystick2CircleC.querySelector('img').src = './assets/image/traffic-light-yellow.png';
      this.joystick2CircleC.querySelector('span').innerHTML = 'C';
    }

    if (taster.answerD) {
      this.joystick2CircleD.querySelector('img').src = './assets/image/traffic-light-blue-active.png';
      let point = this.getMyPoint(3, this.checkInPositionVisibility[1], this.tasterTeam2Active);
      this.joystick2CircleD.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[1].score += point;
    } else {
      this.joystick2CircleD.querySelector('img').src = './assets/image/traffic-light-blue.png';
      this.joystick2CircleD.querySelector('span').innerHTML = 'D';
    }
  }

  team3TasterShowing(taster: Taster) {
    if (taster.answerA) {
      this.joystick3CircleA.querySelector('img').src = './assets/image/traffic-light-green-active.png';
      let point = this.getMyPoint(0, this.checkInPositionVisibility[2], this.tasterTeam3Active);
      this.joystick3CircleA.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[2].score += point;
    } else {
      this.joystick3CircleA.querySelector('img').src = './assets/image/traffic-light-green.png';
      this.joystick3CircleA.querySelector('span').innerHTML = 'A';
    }

    if (taster.answerB) {
      this.joystick3CircleB.querySelector('img').src = './assets/image/traffic-light-red-active.png';
      let point = this.getMyPoint(1, this.checkInPositionVisibility[2], this.tasterTeam3Active);
      this.joystick3CircleB.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[2].score += point;
    } else {
      this.joystick3CircleB.querySelector('img').src = './assets/image/traffic-light-red.png';
      this.joystick3CircleB.querySelector('span').innerHTML = 'B';
    }

    if (taster.answerC) {
      this.joystick3CircleC.querySelector('img').src = './assets/image/traffic-light-yellow-active.png';
      let point = this.getMyPoint(2, this.checkInPositionVisibility[2], this.tasterTeam3Active);
      this.joystick3CircleC.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[2].score += point;
    } else {
      this.joystick3CircleC.querySelector('img').src = './assets/image/traffic-light-yellow.png';
      this.joystick3CircleC.querySelector('span').innerHTML = 'C';
    }

    if (taster.answerD) {
      this.joystick3CircleD.querySelector('img').src = './assets/image/traffic-light-blue-active.png';
      let point = this.getMyPoint(3, this.checkInPositionVisibility[2], this.tasterTeam3Active);
      this.joystick3CircleD.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[2].score += point;
    } else {
      this.joystick3CircleD.querySelector('img').src = './assets/image/traffic-light-blue.png';
      this.joystick3CircleD.querySelector('span').innerHTML = 'D';
    }
  }

  team4TasterShowing(taster: Taster) {
    if (taster.answerA) {
      this.joystick4CircleA.querySelector('img').src = './assets/image/traffic-light-green-active.png';
      let point = this.getMyPoint(0, this.checkInPositionVisibility[3], this.tasterTeam4Active);
      this.joystick4CircleA.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[3].score += point;
    } else {
      this.joystick4CircleA.querySelector('img').src = './assets/image/traffic-light-green.png';
      this.joystick4CircleA.querySelector('span').innerHTML = 'A';
    }

    if (taster.answerB) {
      this.joystick4CircleB.querySelector('img').src = './assets/image/traffic-light-red-active.png';
      let point = this.getMyPoint(1, this.checkInPositionVisibility[3], this.tasterTeam4Active);
      this.joystick4CircleB.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[3].score += point;
    } else {
      this.joystick4CircleB.querySelector('img').src = './assets/image/traffic-light-red.png';
      this.joystick4CircleB.querySelector('span').innerHTML = 'B';
    }

    if (taster.answerC) {
      this.joystick4CircleC.querySelector('img').src = './assets/image/traffic-light-yellow-active.png';
      let point = this.getMyPoint(2, this.checkInPositionVisibility[3], this.tasterTeam4Active);
      this.joystick4CircleC.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[3].score += point;
    } else {
      this.joystick4CircleC.querySelector('img').src = './assets/image/traffic-light-yellow.png';
      this.joystick4CircleC.querySelector('span').innerHTML = 'C';
    }

    if (taster.answerD) {
      this.joystick4CircleD.querySelector('img').src = './assets/image/traffic-light-blue-active.png';
      let point = this.getMyPoint(3, this.checkInPositionVisibility[3], this.tasterTeam4Active);
      this.joystick4CircleD.querySelector('span').innerHTML = point != 0 ? `${point > 0 ? '+' : ''}${point}` : '';
      this.currentGroupTeam[3].score += point;
    } else {
      this.joystick4CircleD.querySelector('img').src = './assets/image/traffic-light-blue.png';
      this.joystick4CircleD.querySelector('span').innerHTML = 'D';
    }
  }

  controlTasterStep1: boolean = false;
  controlTasterStep2: boolean = false;

  pressNextQuestion(): void {
    // if ((this.totalNumberQuestion ) !== this.currentNumberQuestion) {
    if (this.timerQuestion == 0) {
      if (this.nextTasterStep == 0) {
        if (!this.controlTasterStep1) {
          // console.log("prvi taster", new Date());
          this.controlTasterStep1 = true;
          this.addPointTeams(false).then(() => {
            this.nextTasterStep = 1;
            this.controlTasterStep1 = false;
            this.controlTasterStep2 = false;
          });
        }
      } else if (this.nextTasterStep == 1) {
        if (!this.controlTasterStep2) {
          // console.log("drugi taster", new Date());
          this.controlTasterStep2 = true;
          if ((this.totalNumberQuestion - 1) !== this.currentNumberQuestion) {
            this.nextTasterStep = 0;
            this.startNextQuestion();
          } else {
            this.clearIntervals();
            this.controlTasterStep1 = true;
            this.openDialog(this.currentGroupTeam);
          }
        }
      }
    }
  }

  addPointTeams(isDelay: boolean = true): Promise<any> {
    return new Promise((resolve, reject) => {
      this.sortedCalledFunctions.map((item, index) => {
        switch (item.team) {
          case 1:
            this.team1TasterShowing(this.joystickTeam_1);
            break;
          case 2:
            this.team2TasterShowing(this.joystickTeam_2);
            break;
          case 3:
            this.team3TasterShowing(this.joystickTeam_3);
            break;
          case 4:
            this.team4TasterShowing(this.joystickTeam_4);
            break;
        }
      });
      if (isDelay) {
        // console.log("%cAdd POint timeout","color:red");
        let timeOut = setTimeout(() => {
          resolve();
          clearTimeout(timeOut);
        }, 200);
      } else {
        // console.log("%cAdd POint BEZ timeout","color:red");
        resolve();
      }
    });
  }

  startNextQuestion(): void {
    if ((this.currentNumberQuestion - 1) <= this.totalNumberQuestion) {
      this.currentNumberQuestion++;
      this.endQuestion = false;
      this.currentQuestion = this.currentGame.question[this.currentNumberQuestion];
      this.resetTasterIndicator();
      this.startQuestionTimer();
    }
  }

  resetTasterIndicator(): void {
    this.tasterTeam1Active = false;
    this.tasterTeam2Active = false;
    this.tasterTeam3Active = false;
    this.tasterTeam4Active = false;

    this.team1TasterShowing(this.joystickReset);
    this.team2TasterShowing(this.joystickReset);
    this.team3TasterShowing(this.joystickReset);
    this.team4TasterShowing(this.joystickReset);
    this.occupiedPoint = [false, false, false, false];
    this.checkInPosition = 1;
    this.checkInPositionVisibility = [0, 0, 0, 0];
    this.sortedCalledFunctions = [];
  }

  createTestQuestion(): void {

    this.currentGroupTeam = [];
    this.currentGroupTeam.push(new Gamer(1, 'Mita1'));
    this.currentGroupTeam.push(new Gamer(2, 'Mita2'));
    this.currentGroupTeam.push(new Gamer(3, 'Mita3'));
    this.currentGroupTeam.push(new Gamer(4, 'Mita4'));

    let ques: Array<Question>;
    ques = [];
    ques.push(new Question('Da li si sigura 0?', ['da', 'ne', 'mozda', 'ma jok'], 0, 'glupa'));
    ques.push(new Question('Da li si sigura 1?', ['da', 'ne', 'mozda', 'ma jok'], 1, 'glupa'));
    ques.push(new Question('Da li si sigura 2?', ['da', 'ne', 'mozda', 'ma jok'], 2, 'glupa'));
    ques.push(new Question('Da li si sigura 3?', ['da', 'ne', 'mozda', 'ma jok'], 3, 'glupa'));
    ques.push(new Question('Da li si sigura 4?', ['da', 'ne', 'mozda', 'ma jok'], 2, 'glupa'));
    ques.push(new Question('Da li si sigura 5?', ['da', 'ne', 'mozda', 'ma jok'], 1, 'glupa'));
    ques.push(new Question('Da li si sigura 6?', ['da', 'ne', 'mozda', 'ma jok'], 0, 'glupa'));
    ques.push(new Question('Da li si sigura 7?', ['da', 'ne', 'mozda', 'ma jok'], 1, 'glupa'));
    ques.push(new Question('Da li si sigura 8?', ['da', 'ne', 'mozda', 'ma jok'], 3, 'glupa'));
    ques.push(new Question('Da li si sigura 9?', ['da', 'ne', 'mozda', 'ma jok'], 2, 'glupa'));
    ques.push(new Question('Da li si sigura 0?', ['da', 'ne', 'mozda', 'ma jok'], 0, 'glupa'));
    ques.push(new Question('Da li si sigura 1?', ['da', 'ne', 'mozda', 'ma jok'], 1, 'glupa'));
    ques.push(new Question('Da li si sigura 2?', ['da', 'ne', 'mozda', 'ma jok'], 2, 'glupa'));
    ques.push(new Question('Da li si sigura 3?', ['da', 'ne', 'mozda', 'ma jok'], 3, 'glupa'));
    ques.push(new Question('Da li si sigura 4?', ['da', 'ne', 'mozda', 'ma jok'], 2, 'glupa'));
    ques.push(new Question('Da li si sigura 5?', ['da', 'ne', 'mozda', 'ma jok'], 1, 'glupa'));
    ques.push(new Question('Da li si sigura 6?', ['da', 'ne', 'mozda', 'ma jok'], 0, 'glupa'));
    ques.push(new Question('Da li si sigura 7?', ['da', 'ne', 'mozda', 'ma jok'], 1, 'glupa'));
    ques.push(new Question('Da li si sigura 8?', ['da', 'ne', 'mozda', 'ma jok'], 3, 'glupa'));
    ques.push(new Question('Da li si sigura 9?', ['da', 'ne', 'mozda', 'ma jok'], 2, 'glupa'));

    this.currentGame = new Game(ques);
    this.currentQuestion = new Question('', ['', '', '', ''], 0, '');
    this.totalNumberQuestion = this.currentGame.question.length;
    this.currentNumberQuestion = 0;
  }

  selectElementOnHTML(): void {
    document.body.style.overflow = 'hidden';
    this.timerQuestionElement = document.getElementById('timerQuestion');

    this.joystick1CircleA = document.getElementById('team1TasterAnswerA');
    this.joystick1CircleB = document.getElementById('team1TasterAnswerB');
    this.joystick1CircleC = document.getElementById('team1TasterAnswerC');
    this.joystick1CircleD = document.getElementById('team1TasterAnswerD');

    this.joystick2CircleA = document.getElementById('team2TasterAnswerA');
    this.joystick2CircleB = document.getElementById('team2TasterAnswerB');
    this.joystick2CircleC = document.getElementById('team2TasterAnswerC');
    this.joystick2CircleD = document.getElementById('team2TasterAnswerD');

    this.joystick3CircleA = document.getElementById('team3TasterAnswerA');
    this.joystick3CircleB = document.getElementById('team3TasterAnswerB');
    this.joystick3CircleC = document.getElementById('team3TasterAnswerC');
    this.joystick3CircleD = document.getElementById('team3TasterAnswerD');

    this.joystick4CircleA = document.getElementById('team4TasterAnswerA');
    this.joystick4CircleB = document.getElementById('team4TasterAnswerB');
    this.joystick4CircleC = document.getElementById('team4TasterAnswerC');
    this.joystick4CircleD = document.getElementById('team4TasterAnswerD');

    this.timerHTMLElement = document.getElementById("answers-timer");
    this.timerHTMLElement.querySelector('img.answers-timer-alert').style.visibility = 'hidden';
    this.audioSound.src = "../../../assets/audio/sound.mp3";
    this.audioTick.src = "../../../assets/audio/tick.mp3";
    this.audioTaster1.src = "../../../assets/audio/sound_quiz_1.mp3";
    this.audioTaster2.src = "../../../assets/audio/sound_quiz_1.mp3";
    this.audioTaster3.src = "../../../assets/audio/sound_quiz_1.mp3";
    this.audioTaster4.src = "../../../assets/audio/sound_quiz_1.mp3";
    this.audioSound.load();
    this.audioTick.load();
    this.audioTaster1.load();
    this.audioTaster2.load();
    this.audioTaster3.load();
    this.audioTaster4.load();
  }

  playAudioSound() {
    this.audioSound.play();
  }

  stopAudioSound() {
    this.audioSound.pause();
    this.audioSound.currentTime = 0;
  }

  playAudioTick() {
    this.audioTick.play();
  }

  stopAudioTick() {
    this.audioTick.pause();
    this.audioTick.currentTime = 0;
  }

  getMyPoint(myAnswer, checkInMyPosition, tasterActive: boolean): number {
    if (tasterActive) {
      if (this.currentQuestion.correctIndex === myAnswer) {
        switch (checkInMyPosition) {
          case 1:
            this.occupiedPoint[0] = true;
            return this.currentGame.settings.firstCorrectAnswer;
          case 2:
            if (!this.occupiedPoint[0]) {
              this.occupiedPoint[0] = true;
              return this.currentGame.settings.firstCorrectAnswer;
            } else {
              this.occupiedPoint[1] = true;
              return this.currentGame.settings.secondCorrectAnswer;
            }
          case 3:
            if (!this.occupiedPoint[0]) {
              this.occupiedPoint[0] = true;
              return this.currentGame.settings.firstCorrectAnswer;
            } else if (!this.occupiedPoint[1]) {
              this.occupiedPoint[1] = true;
              return this.currentGame.settings.secondCorrectAnswer;
            } else {
              this.occupiedPoint[2] = true;
              return this.currentGame.settings.thirdCorrectAnswer;
            }
          case 4:
            if (!this.occupiedPoint[0]) {
              this.occupiedPoint[0] = true;
              return this.currentGame.settings.firstCorrectAnswer;
            } else if (!this.occupiedPoint[1]) {
              this.occupiedPoint[1] = true;
              return this.currentGame.settings.secondCorrectAnswer;
            } else if (!this.occupiedPoint[2]) {
              this.occupiedPoint[2] = true;
              return this.currentGame.settings.thirdCorrectAnswer;
            } else {
              this.occupiedPoint[3] = true;
              return this.currentGame.settings.fourthCorrectAnswer;
            }
          default:
            return 0;
        }
      } else {
        return this.currentGame.settings.wrongAnswer;
      }
    } else {
      return 0;
    }
  }

  getMyPointForDisplay(myAnswer, checkInMyPosition, tasterActive: boolean): string {
    let rez: string = '';
    if (tasterActive) {
      if (this.currentQuestion.correctIndex == myAnswer) {
        switch (checkInMyPosition) {
          case 1:
            rez = '+' + this.currentGame.settings.firstCorrectAnswer;
            break;
          case 2:
            if (!this.occupiedPoint[0] && !this.occupiedPoint[1] && !this.occupiedPoint[2]) {
              rez = '+' + this.currentGame.settings.firstCorrectAnswer;
            } else {
              rez = '+' + this.currentGame.settings.secondCorrectAnswer;
            }
            // rez = '+' + this.currentGame.settings.secondCorrectAnswer;
            break;
          case 3:
            if (!this.occupiedPoint[0] && !this.occupiedPoint[1] && !this.occupiedPoint[2]) {
              rez = '+' + this.currentGame.settings.firstCorrectAnswer;
            } else if (!this.occupiedPoint[1] && !this.occupiedPoint[2]) {
              rez = '+' + this.currentGame.settings.secondCorrectAnswer;
            } else {
              rez = '+' + this.currentGame.settings.thirdCorrectAnswer;
            }
            // rez = '+' + this.currentGame.settings.thirdCorrectAnswer;
            break;
          case 4:
            if (!this.occupiedPoint[0] && !this.occupiedPoint[1] && !this.occupiedPoint[2]) {
              rez = '+' + this.currentGame.settings.firstCorrectAnswer;
            } else if (!this.occupiedPoint[1] && !this.occupiedPoint[2]) {
              rez = '+' + this.currentGame.settings.secondCorrectAnswer;
            } else if (!this.occupiedPoint[2]) {
              rez = '+' + this.currentGame.settings.thirdCorrectAnswer;
            } else {
              rez = '+' + this.currentGame.settings.fourthCorrectAnswer;
            }
            // rez = '+' + this.currentGame.settings.fourthCorrectAnswer;
            break;
          default:
            rez = '';
        }
      } else {
        rez = String(this.currentGame.settings.wrongAnswer).valueOf();
      }
    }
    return rez ? rez : '';
  }

  openDialog(finishGroupTeam: Array<Gamer>) {
    let dialogRef = this.dialog.open(RangListDialogComponent, {
      width: '40vw',
      data: finishGroupTeam
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.resetTasterIndicator();
        this.chatService.onPressTaster = this;
        this.currentGroupTeam = this.forwardData.groupTeam.slice();
        this.currentGroupTeam = this.currentGroupTeam.map(item => {
          item.score = 0;
          return item;
        });
        this.currentGame = this.forwardData.currentGame;
        this.currentQuestion = new Question('', ['', '', '', ''], 0, '');
        this.totalNumberQuestion = this.currentGame.question.length;
        this.currentNumberQuestion = 0;
        this.timerQuestion = this.currentGame.settings.timeOneQuestion;
        this.controlTasterStep1 = false;
        this.controlTasterStep2 = false;
        this.nextTasterStep = 0;
        this.chatService.messages.subscribe(msg => {
          // console.log(msg);
        });
        let delayStartGame = setTimeout(() => {
          if (this.currentGame != null) {
            this.currentQuestion = this.currentGame.question[this.currentNumberQuestion];
            this.startQuestionTimer();
          }
          clearTimeout(delayStartGame);
        }, 1000);
      } else {
        this.router.navigate(["/start"]);
      }
    });
  }

}
