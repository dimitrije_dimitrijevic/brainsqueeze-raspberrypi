import {Component, OnInit} from '@angular/core';
import {ServerCommunicationService} from "../../services/server-communication.service";
import {Gamer} from "../../model/Gamer";

@Component({
  selector: 'app-rang-list',
  templateUrl: './rang-list.component.html',
  styleUrls: ['./rang-list.component.scss']
})
export class RangListComponent implements OnInit {

  scoresTeams: Array<Gamer>;
  sorted = false;

  constructor(private serverCommunication: ServerCommunicationService) {
    this.serverCommunication.getRangList().subscribe((res: any) => {
      let scoresTeams: Array<Gamer> = res.data;

      let pom;
      if (scoresTeams != null)
        while (!this.sorted) {
          this.sorted = true;
          for (let i = 0; i < scoresTeams.length - 1; i++) {
            // console.log(scoresTeams);
            if (scoresTeams[i].score < scoresTeams[i + 1].score) {
              pom = scoresTeams[i];
              scoresTeams[i] = scoresTeams[i + 1];
              scoresTeams[i + 1] = pom;
              this.sorted = false;
            }
          }
        }
      // console.log(scoresTeams);
      this.scoresTeams = scoresTeams;
    }, (err: any) => {

    });
  }

  ngOnInit() {
  }

  backPage(){
    window.history.back();
  }
}
