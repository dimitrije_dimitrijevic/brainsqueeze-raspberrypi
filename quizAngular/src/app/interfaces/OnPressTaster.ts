import {Taster} from '../model/Taster';

export interface OnPressTaster {

  pressTeam1(taster: Taster): void;

  pressTeam2(taster: Taster): void;

  pressTeam3(taster: Taster): void;

  pressTeam4(taster: Taster): void;

  pressNextQuestion(): void;
}
