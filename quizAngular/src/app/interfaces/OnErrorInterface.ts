/**
 * Created by Mita on 16.5.2018..
 */
export interface OnErrorInterface {
  onError(status: boolean): void;
}
