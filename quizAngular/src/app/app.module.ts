import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {StartPageComponent} from './quiz/start-page/start-page.component';
import {RangListComponent} from './quiz/rang-list/rang-list.component';
import {MainMenuComponent} from './quiz/main-menu/main-menu.component';
import {MainQuizComponent} from './quiz/main-quiz/main-quiz.component';
import {SettingsComponent} from './quiz/settings/settings.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TeamNameComponent} from './quiz/team-name/team-name.component';
import {ChatServiceService} from './services/chat-service.service';
import {WebsocketServiceService} from './services/websocket-service.service';
import {ForwardDataService} from './services/forward-data.service';
import {HttpClientModule} from '@angular/common/http';
import {ServerCommunicationService} from './services/server-communication.service';
import { RangListDialogComponent } from './quiz/rang-list-dialog/rang-list-dialog.component';
import {MatDialogModule} from "@angular/material";

const appRoutes: Routes = [
  {
    path: '',
    component: MainMenuComponent,
  }, {
    path: 'team-name',
    component: TeamNameComponent
  },
  {
    path: 'start-page',
    component: StartPageComponent
  },
  {
    path: 'rang-list',
    component: RangListComponent
  },
  {
    path: 'setting',
    component: SettingsComponent
  },
  {
    path: 'team-name',
    component: TeamNameComponent,
  },
  {
    path: 'main-quiz/:newGame',
    component: MainQuizComponent,
    // pathMatch: 'prefix'
  }, {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  declarations: [
    AppComponent,
    StartPageComponent,
    RangListComponent,
    MainMenuComponent,
    MainQuizComponent,
    SettingsComponent,
    TeamNameComponent,
    RangListDialogComponent,
  ],
  entryComponents: [RangListDialogComponent],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false} // <-- debugging purposes only
    ),
    NgbModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  exports: [
    MatDialogModule,
  ],
  providers: [
    ForwardDataService,
    ServerCommunicationService,
    WebsocketServiceService,
    ChatServiceService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
