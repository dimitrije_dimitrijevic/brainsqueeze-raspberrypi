import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {Observable} from 'rxjs/Observable';
import * as Rx from 'rxjs/Rx';
import {OnPressTaster} from '../interfaces/OnPressTaster';
import {OnErrorInterface} from '../interfaces/OnErrorInterface';
import {Taster} from '../model/Taster';
import {Constants} from "../constants";

@Injectable()
export class WebsocketServiceService {

  // Our socket connection
  private socket;
  private _onPressTaster: OnPressTaster;
  private _error: OnErrorInterface;

  constructor() {
  }

  connect(): Rx.Subject<MessageEvent> {
    this.socket = io(Constants.apiHost, {
      // reconnection: false,
      // reconnectionAttempts: 2
    });


    // We define our observable which will observe any incoming messages
    // from our socket.io server.
    let observable = new Observable(observer => {
      this.socket.on('message', (data) => {
        // console.log('Received message from Websocket Server');
        // observer.next(data);
      });
      this.socket.on('nextQuestion', (data) => {
        if (this._onPressTaster != null)
          this._onPressTaster.pressNextQuestion();
      });
      this.socket.on('tasterGroup1', (data: Taster) => {
        if (this._onPressTaster != null)
          this._onPressTaster.pressTeam1(data);
      });
      this.socket.on('tasterGroup2', (data: Taster) => {
        if (this._onPressTaster != null)
          this._onPressTaster.pressTeam2(data);
      });
      this.socket.on('tasterGroup3', (data: Taster) => {
        if (this._onPressTaster != null)
          this._onPressTaster.pressTeam3(data);
      });
      this.socket.on('tasterGroup4', (data: Taster) => {
        if (this._onPressTaster != null)
          this._onPressTaster.pressTeam4(data);
      });


      // this.socket.io.on('reconnect', () => {
      //   console.log("reconnect");
      // });
      // this.socket.io.on('reconnect_attempt', () => {
      //   console.log("reconnect_attempt");
      // });
      //
      // this.socket.io.on('reconnect_failed', () => {
      //   console.log("reconnect_failed");
      // });
      // this.socket.io.on('reconnect_error', () => {
      //   console.log("reconnect_error");
      // });
      // this.socket.io.on('reconnecting', () => {
      //   console.log("reconnecting");
      // });
      // this.socket.io.on('connect_error', () => {
      //   console.log("connect_error");
      // });
      // this.socket.io.on('connect_timeout', () => {
      //   console.log("connect_timeout");
      // });
      // this.socket.io.on('connecting', () => {
      //   console.log("connecting");
      // });


      return () => {
        this.socket.disconnect();
      };
    });


    // We define our Observer which will listen to messages
    // from our other components and send messages back to our
    // socket server whenever the `next()` method is called.
    let observer = {
      next: (data: Object) => {
        this.socket.emit('message', JSON.stringify(data));
      },
    };

    // we return our Rx.Subject which is a combination
    // of both an observer and observable.
    return Rx.Subject.create(observer, observable);
  }


  set onPressTaster(value: OnPressTaster) {
    this._onPressTaster = value;
  }


  set onError(value: OnErrorInterface) {
    this._error = value;
  }
}
