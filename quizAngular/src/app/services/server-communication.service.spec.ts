import { TestBed, inject } from '@angular/core/testing';

import { ServerComunicationService } from './server-communication.service';

describe('ServerComunicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServerComunicationService]
    });
  });

  it('should be created', inject([ServerComunicationService], (service: ServerComunicationService) => {
    expect(service).toBeTruthy();
  }));
});
