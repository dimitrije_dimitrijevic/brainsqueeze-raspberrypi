import { TestBed, inject } from '@angular/core/testing';

import { ForwardDataService } from './forward-data.service';

describe('ForwardDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ForwardDataService]
    });
  });

  it('should be created', inject([ForwardDataService], (service: ForwardDataService) => {
    expect(service).toBeTruthy();
  }));
});
