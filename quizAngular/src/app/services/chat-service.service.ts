import {Injectable} from '@angular/core';
import {WebsocketServiceService} from './websocket-service.service';
import {Observable, Subject} from 'rxjs/Rx';
import {OnPressTaster} from '../interfaces/OnPressTaster';
import {OnErrorInterface} from "../interfaces/OnErrorInterface";

@Injectable()
export class ChatServiceService {

  messages: Subject<any>;
  private _onPressTaster: OnPressTaster;
  private _error: OnErrorInterface;

  // Our constructor calls our wsService connect method
  constructor(private wsService: WebsocketServiceService) {
    this.messages = <Subject<any>>wsService
      .connect()
      .map((response: any): any => {
        return response;
      });
  }


  // Our simplified interface for sending
  // messages back to our socket.io server
  sendMsg(msg) {
    this.messages.next(msg);
  }


  set onPressTaster(value: OnPressTaster) {
    this._onPressTaster = value;
    this.wsService.onPressTaster = value;
  }


  set onError(value: OnErrorInterface) {
    this._error = value;
    this.wsService.onError = value;
  }
}
