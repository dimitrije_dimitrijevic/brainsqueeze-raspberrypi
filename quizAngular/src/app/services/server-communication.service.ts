import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";
import {Constants} from '../constants';
import {Settings} from "../model/Settings";

@Injectable({
  providedIn: 'root'
})
export class ServerCommunicationService {


  constructor(private httpClient: HttpClient) {
  }

  public syncQuestion(): Observable<any> {
    return Observable.create(observer => {
      this.httpClient.get(Constants.apiHost + 'api/sync-question').timeout(30000).subscribe((res: any) => {
        observer.next({success: true});
        observer.complete();
      }, (err) => {
        console.error(err);
        observer.error({success: false});
        observer.complete();
      });
    });
  }

  public postScoreLastGame(scores): Observable<any> {
    return Observable.create(observer => {
      this.httpClient.post(Constants.apiHost + 'api/scores', scores).subscribe((res: any) => {
        observer.next(res);
        observer.complete();
      }, (err) => {
        observer.error({success: false});
        observer.complete();
      });
    });
  }

  public postSetSettings(settings: Settings): Observable<any> {
    return Observable.create(observer => {
      this.httpClient.post(Constants.apiHost + 'api/settings', settings).subscribe((res: any) => {
        observer.next({success: true});
        observer.complete();
      }, (err) => {
        observer.error({success: false});
        observer.complete();
      });
    });
  }

  public postDownloadLogo(url: string): Observable<any> {
    return Observable.create(observer => {
      this.httpClient.post(Constants.apiHost + 'api/download-logo', {logo_url: url}).subscribe((res: any) => {
        observer.next({success: true});
        observer.complete();
      }, (err) => {
        observer.error({success: false});
        observer.complete();
      });
    });
  }

  public getSettings(): Observable<any> {
    return Observable.create(observer => {
      this.httpClient.get(Constants.apiHost + 'api/settings').subscribe((res: Settings) => {
        observer.next({success: true, settings: res});
        observer.complete();
      }, (err) => {
        observer.error({success: false});
        observer.complete();
      });
    });
  }

  public getRangList(): Observable<any> {
    return Observable.create(observer => {
      this.httpClient.get(Constants.apiHost + 'api/scores').subscribe((res: any) => {
        observer.next(res);
        observer.complete();
      }, (err) => {
        observer.error({success: false});
        observer.complete();
      });
    });
  }

  public getGameQuestion(): Observable<any> {
    return Observable.create(observer => {
      this.httpClient.get(Constants.apiHost + 'api/game-question').timeout(10000).subscribe((res: any) => {
        observer.next({success: true, game: res.data});
        observer.complete();
      }, (err: any) => {
        console.error(err);
        observer.error({success: false});
        observer.complete();
      });
    });
  }
}
