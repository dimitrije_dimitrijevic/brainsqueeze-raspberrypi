import {Injectable} from '@angular/core';
import {Gamer} from '../model/Gamer';
import {Game} from "../model/Game";

@Injectable()
export class ForwardDataService {

  private _groupTeam: Array<Gamer>;
  private _currentGame: Game;
  private _startNewGame: string;

  constructor() {
  }


  get groupTeam(): Array<Gamer> {
    return this._groupTeam;
  }

  set groupTeam(value: Array<Gamer>) {
    this._groupTeam = value;
  }


  get currentGame(): Game {
    return this._currentGame;
  }

  set currentGame(value: Game) {
    this._currentGame = value;
  }


  get startNewGame(): string {
    return this._startNewGame;
  }

  set startNewGame(value: string) {
    this._startNewGame = value;
  }
}
