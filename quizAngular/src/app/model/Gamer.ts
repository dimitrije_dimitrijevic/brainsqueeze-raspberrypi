export class Gamer {
  private _teamId: number;
  private _name: string;
  private _score: number = 0;


  constructor(id: number, name: string) {
    this._teamId = id;
    this._name = name;
    this._score = 0;
  }

  get teamId(): number {
    return this._teamId;
  }

  set teamId(value: number) {
    this._teamId = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get score(): number {
    return this._score;
  }

  set score(value: number) {
    this._score = value;
  }

  getGamer(): any {
    return {
      teamId: this._teamId,
      name: this._name,
      score: this._score
    }
  }
}
