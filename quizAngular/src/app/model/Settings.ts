export class Settings {
  private _timeOneQuestion: number = 0;
  private _numberQuestion: number = 0;
  private _firstCorrectAnswer: number = 0;
  private _secondCorrectAnswer: number = 0;
  private _thirdCorrectAnswer: number = 0;
  private _fourthCorrectAnswer: number = 0;
  private _wrongAnswer: number = 0;
  private _category: string[];
  private _categories: string[];
  private _logoImgUrl: string;

  get timeOneQuestion(): number {
    return this._timeOneQuestion;
  }

  set timeOneQuestion(value: number) {
    this._timeOneQuestion = value;
  }

  get numberQuestion(): number {
    return this._numberQuestion;
  }

  set numberQuestion(value: number) {
    this._numberQuestion = value;
  }

  get firstCorrectAnswer(): number {
    return this._firstCorrectAnswer;
  }

  set firstCorrectAnswer(value: number) {
    this._firstCorrectAnswer = value;
  }

  get secondCorrectAnswer(): number {
    return this._secondCorrectAnswer;
  }

  set secondCorrectAnswer(value: number) {
    this._secondCorrectAnswer = value;
  }

  get thirdCorrectAnswer(): number {
    return this._thirdCorrectAnswer;
  }

  set thirdCorrectAnswer(value: number) {
    this._thirdCorrectAnswer = value;
  }

  get fourthCorrectAnswer(): number {
    return this._fourthCorrectAnswer;
  }

  set fourthCorrectAnswer(value: number) {
    this._fourthCorrectAnswer = value;
  }

  get wrongAnswer(): number {
    return this._wrongAnswer;
  }

  set wrongAnswer(value: number) {
    this._wrongAnswer = value;
  }

  get category(): string[] {
    return this._category;
  }

  set category(value: string[]) {
    this._category = value;
  }

  get categories(): string[] {
    return this._categories;
  }

  set categories(value: string[]) {
    this._categories = value;
  }


  get logoImgUrl(): string {
    return this._logoImgUrl;
  }

  set logoImgUrl(value: string) {
    this._logoImgUrl = value;
  }
}
