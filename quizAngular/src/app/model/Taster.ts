export class Taster {
  private _answerA = false;
  private _answerB = false;
  private _answerC = false;
  private _answerD = false;


  constructor(answerA: boolean, answerB: boolean, answerC: boolean, answerD: boolean) {
    this._answerA = answerA;
    this._answerB = answerB;
    this._answerC = answerC;
    this._answerD = answerD;
  }

  get answerA(): boolean {
    return this._answerA;
  }

  set answerA(value: boolean) {
    this._answerA = value;
  }

  get answerB(): boolean {
    return this._answerB;
  }

  set answerB(value: boolean) {
    this._answerB = value;
  }

  get answerC(): boolean {
    return this._answerC;
  }

  set answerC(value: boolean) {
    this._answerC = value;
  }

  get answerD(): boolean {
    return this._answerD;
  }

  set answerD(value: boolean) {
    this._answerD = value;
  }
}
