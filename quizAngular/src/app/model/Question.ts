export class Question {
  private _question: string;
  private _answers: Array<string>;
  private _correctIndex: number;
  private _category: string;

  constructor(question: string, answer: Array<string>, correctIndex: number, category: string) {
    this._question = question;
    this._answers = answer;
    this._correctIndex = correctIndex;
    this._category = category;
  }

  get question(): string {
    return this._question;
  }

  set question(value: string) {
    this._question = value;
  }

  get answers(): Array<string> {
    return this._answers;
  }

  set answers(value: Array<string>) {
    this._answers = value;
  }

  get correctIndex(): number {
    return this._correctIndex;
  }

  set correctIndex(value: number) {
    this._correctIndex = value;
  }

  get category(): string {
    return this._category;
  }

  set category(value: string) {
    this._category = value;
  }
}
