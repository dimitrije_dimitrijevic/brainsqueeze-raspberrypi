import {Question} from './Question';
import {Settings} from "./Settings";

export class Game {

  private _question: Array<Question>;
  private _settings: Settings;

  constructor(question: Array<Question>) {
    this._question = question;
  }

  get question(): Array<Question> {
    return this._question;
  }

  set question(value: Array<Question>) {
    this._question = value;
  }


  get settings(): Settings {
    return this._settings;
  }

  set settings(value: Settings) {
    this._settings = value;
  }
}
