import {Component} from '@angular/core';
import {NgbProgressbarConfig} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  progressValue: number = 10;
  splashInterval: any;
  quiz: boolean = false;

  constructor(private config: NgbProgressbarConfig) {
    config.max = 100;
    config.striped = true;
    config.animated = true;
    config.type = 'warning';
    config.height = '70px';

    this.splashInterval = setInterval(() => {
      this.progressValue++;
      if (this.progressValue > 120) {
        clearInterval(this.splashInterval);
        this.quiz = true;
      }
    }, 80);
  }

}
