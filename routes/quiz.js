const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const http = require('http');
const https = require('https');
const constants = require("../controller/constants"),
    MongoClient = require('mongodb').MongoClient,
    ObjectId = require('mongodb').ObjectID,
    assert = require('assert'),
    request = require('request');

let defaultSettings = {
    "timeOneQuestion": 10,
    "numberQuestion": 10,
    "firstCorrectAnswer": 5,
    "secondCorrectAnswer": 3,
    "thirdCorrectAnswer": 1,
    "fourthCorrectAnswer": 0,
    "wrongAnswer": -2,
    "key": "settings",
    "category": "Umetnost",
    "logoImgUrl":"http://quiz.itcentar.rs/logo-big.png"
};

router.get('/scores', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);

            getScores(db, (err, data) => {
                res.status(err ? 503 : 200).json({
                    err: err,
                    data
                });
                res.end();
                client.close();

            });
        } catch (err) {
            res.status(503).json({
                err: true
            });
            res.end();
        }
    });
});

router.post('/scores', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);
            insertScores(db, req.body, (err, data) => {
                res.status(err ? 503 : 200).json({
                    err: err,
                    data
                });
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                err: true
            });
            res.end();
        }
    });
});

router.get('/settings', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);

            getSettings(db).then((data) => {
                res.status(200).json(data);
                res.end();
                client.close();
            });


        } catch (err) {
            res.status(503).json({
                err: true
            });
            res.end();
        }
    });
});

router.post('/settings', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);
            updateSettings(db, req.body, (err, data) => {
                res.status(err ? 503 : 200).json({
                    err: err,
                    data
                });
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                err: true
            });
            res.end();
        }
    });
});

router.get('/sync-question', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);
            syncQuestion(db).then((resSync) => {
                res.status(200).json({
                    err: resSync
                });
                res.end();
            }).catch((err) => {
                res.status(503).json({
                    err: true,
                    message: err
                });
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                err: true
            });
            res.end();
        }
    });
});

router.get('/game-question', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            const db = client.db(constants.mongodbName);
            getGameQuestion(db).then((data) => {
                res.status(200).json({
                    data
                });
                res.end();
            }).catch((err) => {
                res.status(503).json({
                    err: err,
                });
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                err: true
            });
            res.end();
        }
    });
});

router.get('/categories', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);
            getCategories(db).then((categories) => {
                res.status(200).json({
                    categories
                });
                res.end();
            }).catch((err) => {
                res.status(503).json({
                    err: err
                });
                res.end();
            });

        } catch (err) {
            res.status(503).json({
                err: true,
                message: err
            });
            res.end();
        }
    });
});

router.post('/download-logo', (req, res, next) => {
    let file = fs.createWriteStream(path.join(__dirname, '../public/images/') + 'main-logo.png');
    if (req.body['logo_url'].match('http://')) {
        http.get(req.body['logo_url'], function (response) {
            response.pipe(file);
            file.on('finish', function () {
                res.status(200);
                res.end();
            });
            file.on('error', function (err) {
                res.status(503).json({
                    err: err
                });
                res.end();
            });
        });
    } else if (req.body['logo_url'].match('https://')) {
        https.get(req.body['logo_url'], function (response) {
            response.pipe(file);
            file.on('finish', function () {
                res.status(200);
                res.end();
            });
            file.on('error', function (err) {
                res.status(503).json({
                    err: err
                });
                res.end();
            });
        });
    }
});

module.exports = router;

const insertScores = function (db, scores, callback) {
    const collection = db.collection(constants.mongodbCollectionScores);
    collection.insertMany(scores, (err, result) => {
        try {
            assert.equal(err, null);
            // console.log("Inserted user in collection");
            return callback(false, result);
        } catch (e) {
            return callback(true, result);
        }
    });
};

const getScores = function (db, callback) {
    const collection = db.collection(constants.mongodbCollectionScores);
    collection.find({}).toArray(function (err, users) {
        try {
            assert.equal(err, null);
            if (users.length > 0) {
                callback(false, users);
            } else {
                return callback(false);
            }
        } catch (err) {
            return callback(true);
        }
    });
};

const updateSettings = function (db, settings, callback) {
    const collection = db.collection(constants.mongodbCollectionSettings);

    checkConfigExists(db, (exists) => {
        if (!exists) {
            collection.insertOne({...settings, key: 'settings'}, (err, result) => {
                try {
                    assert.equal(err, null);
                    return callback(false, result);
                } catch (e) {
                    return callback(true, result);
                }
            });
        } else {
            collection.findOneAndUpdate({key: 'settings'}, {$set: settings}, (err, result) => {
                try {
                    assert.equal(err, null);
                    return callback(false, result);
                } catch (e) {
                    return callback(true, result);
                }
            });
        }
    });

};

const updateOldQuestions = function (db, oldQuestionsId) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(constants.mongodbCollectionSettings);
        collection.findOne({key: 'oldQuestions'}, function (err, docs) {
            if (docs) {
                collection.findOneAndUpdate({key: 'oldQuestions'}, {$push: {oldQuestions: {$each: oldQuestionsId}}}, (err, result) => {
                    try {
                        assert.equal(err, null);
                        resolve(false, result);
                    } catch (e) {
                        reject(true, result);
                    }
                });
            } else {
                collection.insertOne({key: 'oldQuestions', oldQuestions: oldQuestionsId}, (err, result) => {
                    try {
                        assert.equal(err, null);
                        resolve(false, result);
                    } catch (e) {
                        reject(true, result);
                    }
                });
            }
        });
    });
};

const getOldQuestionsId = function (db) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(constants.mongodbCollectionSettings);
        collection.findOne({key: 'oldQuestions'}, function (err, doc) {
            try {
                db.collection(constants.mongodbCollectionQuestion).count().then(count => {
                    if (doc.oldQuestions.length > count - 10) {
                        collection.remove({key: 'oldQuestions'});
                        resolve([]);
                    } else {
                        resolve(doc.oldQuestions);
                    }
                }).catch(err => {
                    resolve([]);
                });
            } catch (err) {
                resolve([]);
            }
        });
    });
};

const getSettings = function (db) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(constants.mongodbCollectionSettings);
        collection.findOne({key: 'settings'}, function (err, settings) {
            try {
                getCategories(db).then((categories) => {
                    resolve({...settings, categories: categories});
                });
            } catch (err) {
                reject(defaultSettings);
            }
        });
    });
};

const checkConfigExists = function (db, callback) {
    const collection = db.collection(constants.mongodbCollectionSettings);
    collection.findOne({key: 'settings'}, function (err, docs) {
        if (docs) {
            return callback(true);
        } else {
            return callback(false);
        }
    });
};

const syncQuestion = function (db) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(constants.mongodbCollectionQuestion);
        request.get({
            // headers: {'Content-Type': 'application/json'},
            url: constants.brainSqueezePortalHost + 'questions',
            // form: JSON.stringify({
            //     'code': "sega",
            //     'flag': "syncQuestion"
            // })
        }, (err, res, body) => {
            if (res && res.statusCode >= 200 || res.statusCode <= 300) {
                collection.removeMany();
                let questionsBidy = JSON.parse(body);
                let questionsSync = [];
                questionsBidy.questions.difficulty1.map((item, index) => {
                    questionsSync.push(item);
                });
                questionsBidy.questions.difficulty2.map((item, index) => {
                    questionsSync.push(item);
                });
                questionsBidy.questions.difficulty3.map((item, index) => {
                    questionsSync.push(item);
                });
                collection.insertMany(questionsSync, (err, doc) => {
                    if (doc) {
                        resolve({s: false});
                    } else {
                        reject({e: true});
                    }
                });
            } else {
                reject({e: true});
            }
        });
    });
};

const getGameQuestion = function (db) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(constants.mongodbCollectionQuestion);
        getSettings(db).then((settings) => {
            getOldQuestionsId(db).then((oldQuestionsId) => {
                collection.find({
                    category: {$in: settings.category},
                    _id: {$nin: oldQuestionsId}
                }).toArray(function (err, questions) {
                    try {
                        // console.log('PITANJA', questions, settings);
                        assert.equal(err, null);
                        if (questions.length > 0) {
                            questions.shuffleArray();
                            getQuestion(questions, settings.numberQuestion).then((question) => {
                                // console.log(question, settings);
                                let questionsId = [];
                                questionsId = question.map(item => item._id);
                                updateOldQuestions(db, questionsId).then(() => {
                                });
                                resolve({
                                    question: question,
                                    settings: settings
                                });
                            }).catch((err) => {
                                reject({question: 'no_exists'});
                            });
                        } else {
                            reject({question: 'no_exists'});
                        }
                    } catch (err) {
                        reject({question: 'err'});
                    }
                });
            });
        });
    });
};

const getCategories = function (db) {
    return new Promise((resolve, reject) => {
        const collection = db.collection(constants.mongodbCollectionQuestion);
        collection.find().toArray(function (err, questions) {
            try {
                let categories = [];
                questions.map((item) => {
                    categories.push(item.category);
                });
                resolve(categories.removeDuplicates());
            } catch (err) {
                reject(err);
            }
        });
    });
};

function getQuestion(array, number) {
    return new Promise((reject, resolve) => {
        let ar = [];
        // for (let i = 0; i < number; i++) {
        try {
            while (ar.length < number) {
                let j = Math.floor(Math.random() * array.length - 1);
                let question = array[j];
                if (!existsQuestionInArray(ar, question)) {
                    //TODO shuffle answers
                    let correctIndexOldPosition = question.answers[question.correctIndex];
                    // console.log("Pre mesanja");
                    // console.log(correctIndexOldPosition + " - " + question.correctIndex);
                    question.answers = question.answers.shuffleArray();
                    question.correctIndex = getCorrectAnswerPosition(question.answers, correctIndexOldPosition);
                    // console.log("POSLE mesanja");
                    // console.log(question.answers[question.correctIndex] + " - " + question.correctIndex);
                    ar.push(question);
                }
            }
            reject(ar);
        } catch (err) {
            resolve(err);
        }
    });
}

function existsQuestionInArray(array, idQuestion) {
    let exists = false;
    array.map((item) => {
        exists = item._id === idQuestion;
    });
    return exists;
}

function getCorrectAnswerPosition(answers, correctAnswer) {
    let position = -1;
    // console.log(correctAnswer);
    for (let i = 0; i < answers.length; i++) {
        if (answers[i] === correctAnswer) {
            position = i;
        }
    }
    // console.log(position);
    return position;
}

Array.prototype.shuffleArray = function () {
    // for (let i = array.length - 1; i > 0; i--) {
    //     let j = Math.floor(Math.random() * (i + 1));
    //     [array[i], array[j]] = [array[j], array[i]];
    // }
    return this.map((a) => [Math.random(), a]).sort((a, b) => a[0] - b[0]).map((a) => a[1]);
};

Array.prototype.shuffleArraySecond = function () {
    let input = this;

    for (let i = input.length - 1; i >= 0; i--) {

        let randomIndex = Math.floor(Math.random() * (i + 1));
        let itemAtIndex = input[randomIndex];

        input[randomIndex] = input[i];
        input[i] = itemAtIndex;
    }
    return input;
};

Array.prototype.removeDuplicates = function () {
    let unique_array = [];
    for (let i = 0; i < this.length; i++) {
        if (unique_array.indexOf(this[i]) === -1) {
            unique_array.push(this[i])
        }
    }
    return unique_array
};