/**
 * Created by Mita on 3.5.2018..
 */
const DEVELOP_MODE = true;
const MONGODB_DN_NAME = "rpi_brain_squeeze";
const MONGODB_URL = "mongodb://localhost:27017/";

module.exports = {
    developedMode: DEVELOP_MODE,
    brainSqueezePortalHost: "http://quiz.itcentar.rs/api/",
    appPort: 443,
    uniqueToken: "D/6H{x7ueK?%[w/2p4_G_*V",
    mongodbUrlDB: MONGODB_URL,
    mongodbName: MONGODB_DN_NAME,
    mongodbCollectionScores: 'scores',
    mongodbCollectionQuestion: 'questions',
    mongodbCollectionSettings: 'settings',
};