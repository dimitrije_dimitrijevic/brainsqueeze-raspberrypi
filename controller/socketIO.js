const io = require('socket.io');
const constants = require("../controller/constants");
let socketIo;
let gpio;
let Gpio;
let timeOutResetFlagControl;
let flagResetControl = false;
let buttonControl,
    buttonTeam_1_taster_A,
    buttonTeam_1_taster_B,
    buttonTeam_1_taster_C,
    buttonTeam_1_taster_D,
    buttonTeam_2_taster_A,
    buttonTeam_2_taster_B,
    buttonTeam_2_taster_C,
    buttonTeam_2_taster_D,
    buttonTeam_3_taster_A,
    buttonTeam_3_taster_B,
    buttonTeam_3_taster_C,
    buttonTeam_3_taster_D,
    buttonTeam_4_taster_A,
    buttonTeam_4_taster_B,
    buttonTeam_4_taster_C,
    buttonTeam_4_taster_D;

const constantsNumberGPIO = {
    BUTTON_CONTROL: 26,
    TEAM_1_TASTER_A: 16,
    TEAM_1_TASTER_B: 4,
    TEAM_1_TASTER_C: 14,
    TEAM_1_TASTER_D: 15,
    TEAM_2_TASTER_A: 18,
    TEAM_2_TASTER_B: 17,
    TEAM_2_TASTER_C: 27,
    TEAM_2_TASTER_D: 22,
    TEAM_3_TASTER_A: 23,
    TEAM_3_TASTER_B: 24,
    TEAM_3_TASTER_C: 10,
    TEAM_3_TASTER_D: 9,
    TEAM_4_TASTER_A: 11,
    TEAM_4_TASTER_B: 25,
    TEAM_4_TASTER_C: 8,
    TEAM_4_TASTER_D: 7,
};


if (!constants.developedMode) {
    Gpio = require('tm-onoff').Gpio;

    buttonControl = new Gpio(constantsNumberGPIO.BUTTON_CONTROL, 'in', 'rising');
    buttonTeam_1_taster_A = new Gpio(constantsNumberGPIO.TEAM_1_TASTER_A, 'in', 'rising');
    buttonTeam_1_taster_B = new Gpio(constantsNumberGPIO.TEAM_1_TASTER_B, 'in', 'rising');
    buttonTeam_1_taster_C = new Gpio(constantsNumberGPIO.TEAM_1_TASTER_C, 'in', 'rising');
    buttonTeam_1_taster_D = new Gpio(constantsNumberGPIO.TEAM_1_TASTER_D, 'in', 'rising');

    buttonTeam_2_taster_A = new Gpio(constantsNumberGPIO.TEAM_2_TASTER_A, 'in', 'rising');
    buttonTeam_2_taster_B = new Gpio(constantsNumberGPIO.TEAM_2_TASTER_B, 'in', 'rising');
    buttonTeam_2_taster_C = new Gpio(constantsNumberGPIO.TEAM_2_TASTER_C, 'in', 'rising');
    buttonTeam_2_taster_D = new Gpio(constantsNumberGPIO.TEAM_2_TASTER_D, 'in', 'rising');

    buttonTeam_3_taster_A = new Gpio(constantsNumberGPIO.TEAM_3_TASTER_A, 'in', 'rising');
    buttonTeam_3_taster_B = new Gpio(constantsNumberGPIO.TEAM_3_TASTER_B, 'in', 'rising');
    buttonTeam_3_taster_C = new Gpio(constantsNumberGPIO.TEAM_3_TASTER_C, 'in', 'rising');
    buttonTeam_3_taster_D = new Gpio(constantsNumberGPIO.TEAM_3_TASTER_D, 'in', 'rising');

    buttonTeam_4_taster_A = new Gpio(constantsNumberGPIO.TEAM_4_TASTER_A, 'in', 'rising');
    buttonTeam_4_taster_B = new Gpio(constantsNumberGPIO.TEAM_4_TASTER_B, 'in', 'rising');
    buttonTeam_4_taster_C = new Gpio(constantsNumberGPIO.TEAM_4_TASTER_C, 'in', 'rising');
    buttonTeam_4_taster_D = new Gpio(constantsNumberGPIO.TEAM_4_TASTER_D, 'in', 'rising');

    buttonControl.watch(function (err, value) {
        if (err) {
            exit(-100);
        }
        if (value && !flagResetControl) {
            sendMessageSocketIo(-100, {next: true});
            flagResetControl = true;
            timeOutResetFlagControl = setTimeout(() => {

                flagResetControl = false;
                clearTimeout(timeOutResetFlagControl);
            }, 2000);
        }
    });

    buttonTeam_1_taster_A.watch(function (err, value) {
        if (err) {
            exit(1, 'A');
        }
        //console.log("A", err, value);
        if (value) {
            sendMessageSocketIo(1, Taster(true, false, false, false));
        }
    });

    buttonTeam_1_taster_B.watch(function (err, value) {
        if (err) {
            exit(1, 'B');
        }
        //console.log("B", err, value);
        if (value) {
            sendMessageSocketIo(1, Taster(false, true, false, false));
        }
    });

    buttonTeam_1_taster_C.watch(function (err, value) {
        if (err) {
            exit(1, 'C');
        }
        //console.log("C", err, value);
        if (value) {
            sendMessageSocketIo(1, Taster(false, false, true, false));
        }
    });

    buttonTeam_1_taster_D.watch(function (err, value) {
        if (err) {
            exit(1, 'D');
        }
        //console.log("D", err, value);
        if (value) {
            sendMessageSocketIo(1, Taster(false, false, false, true));
        }
    });

    buttonTeam_2_taster_A.watch(function (err, value) {
        if (err) {
            exit(2, 'A');
        }
        //console.log("A", err, value);
        if (value) {
            sendMessageSocketIo(2, Taster(true, false, false, false));
        }
    });

    buttonTeam_2_taster_B.watch(function (err, value) {
        if (err) {
            exit(2, 'B');
        }
        //console.log("B", err, value);
        if (value) {
            sendMessageSocketIo(2, Taster(false, true, false, false));
        }
    });

    buttonTeam_2_taster_C.watch(function (err, value) {
        if (err) {
            exit(1, 'C');
        }
        //console.log("C", err, value);
        if (value) {
            sendMessageSocketIo(2, Taster(false, false, true, false));
        }
    });

    buttonTeam_2_taster_D.watch(function (err, value) {
        if (err) {
            exit(1, 'D');
        }
        //console.log("D", err, value);
        if (value) {
            sendMessageSocketIo(2, Taster(false, false, false, true));
        }
    });

    buttonTeam_3_taster_A.watch(function (err, value) {
        if (err) {
            exit(3, 'A');
        }
        //console.log("A", err, value);
        if (value) {
            sendMessageSocketIo(3, Taster(true, false, false, false));
        }
    });

    buttonTeam_3_taster_B.watch(function (err, value) {
        if (err) {
            exit(3, 'B');
        }
        //console.log("B", err, value);
        if (value) {
            sendMessageSocketIo(3, Taster(false, true, false, false));
        }
    });

    buttonTeam_3_taster_C.watch(function (err, value) {
        if (err) {
            exit(3, 'C');
        }
        //console.log("C", err, value);
        if (value) {
            sendMessageSocketIo(3, Taster(false, false, true, false));
        }
    });

    buttonTeam_3_taster_D.watch(function (err, value) {
        if (err) {
            exit(3, 'D');
        }
        //console.log("D", err, value);
        if (value) {
            sendMessageSocketIo(3, Taster(false, false, false, true));
        }
    });

    buttonTeam_4_taster_A.watch(function (err, value) {
        if (err) {
            exit(4, 'A');
        }
        //console.log("A", err, value);
        if (value) {
            sendMessageSocketIo(4, Taster(true, false, false, false));
        }
    });

    buttonTeam_4_taster_B.watch(function (err, value) {
        if (err) {
            exit(4, 'B');
        }
        //console.log("B", err, value);
        if (value) {
            sendMessageSocketIo(4, Taster(false, true, false, false));
        }
    });

    buttonTeam_4_taster_C.watch(function (err, value) {
        if (err) {
            exit(4, 'C');
        }
        //console.log("C", err, value);
        if (value) {
            sendMessageSocketIo(4, Taster(false, false, true, false));
        }
    });

    buttonTeam_4_taster_D.watch(function (err, value) {
        if (err) {
            exit(4, 'D');
        }
        //console.log("D", err, value);
        if (value) {
            sendMessageSocketIo(4, Taster(false, false, false, true));
        }
    });
}

process.on('SIGINT', exit);
function exit(teamTaster, colorTaster) {
    switch (teamTaster) {
        case 1:
            switch (colorTaster) {
                case 'A':
                    buttonTeam_1_taster_A.unexport();
                    break;
                case 'B':
                    buttonTeam_1_taster_B.unexport();
                    break;
                case 'C':
                    buttonTeam_1_taster_C.unexport();
                    break;
                case 'D':
                    buttonTeam_1_taster_D.unexport();
                    break;
            }
            break;
        case 2:
            switch (colorTaster) {
                case 'A':
                    buttonTeam_2_taster_A.unexport();
                    break;
                case 'B':
                    buttonTeam_2_taster_B.unexport();
                    break;
                case 'C':
                    buttonTeam_2_taster_C.unexport();
                    break;
                case 'D':
                    buttonTeam_2_taster_D.unexport();
                    break;
            }
            break;
        case 3:
            switch (colorTaster) {
                case 'A':
                    buttonTeam_3_taster_A.unexport();
                    break;
                case 'B':
                    buttonTeam_3_taster_B.unexport();
                    break;
                case 'C':
                    buttonTeam_3_taster_C.unexport();
                    break;
                case 'D':
                    buttonTeam_3_taster_D.unexport();
                    break;
            }
            break;
        case 4:
            switch (colorTaster) {
                case 'A':
                    buttonTeam_4_taster_A.unexport();
                    break;
                case 'B':
                    buttonTeam_4_taster_B.unexport();
                    break;
                case 'C':
                    buttonTeam_4_taster_C.unexport();
                    break;
                case 'D':
                    buttonTeam_4_taster_D.unexport();
                    break;
            }
            break;
        case -100:
            buttonControl.unexport();
            break;
    }
    process.exit();
}

function Taster(answerA, answerB, answerC, answerD) {
    return {answerA: answerA, answerB: answerB, answerC: answerC, answerD: answerD};
}

function sendMessageSocketIo(teamTaster, taster) {
    if (socketIo) {
        switch (teamTaster) {
            case 1:
                socketIo.emit('tasterGroup1', taster);
                break;
            case 2:
                socketIo.emit('tasterGroup2', taster);
                break;
            case 3:
                socketIo.emit('tasterGroup3', taster);
                break;
            case 4:
                socketIo.emit('tasterGroup4', taster);
                break;
            case -100:
                socketIo.emit('nextQuestion', taster);
                break
        }
    }
}

module.exports.setServer = function (server) {
    socketIo = io.listen(server);

    socketIo.on('connection', function (socket) {
        //console.log("User " + socket.handshake.address + " connected!");

        // setInterval(() => {
        //     socketIo.emit('error', false);
        // }, 1000);


        if (constants.developedMode) {
            socket.on('message', (message) => {
                // //console.log("Message Received: " + message);
                // socketIo.emit('message', {type: 'new-message', text: message});
                // sendMessageSocketIo(2, Taster(false, true, false, false));
                // sendMessageSocketIo(1, Taster(true, false, false, false));
                // sendMessageSocketIo(4, Taster(false, false, false, true));
                // sendMessageSocketIo(3, Taster(false, false, true, false));
                sendMessageSocketIo(2, Taster(false, true, false, false));
                sendMessageSocketIo(1, Taster(false, true, false, false));
                sendMessageSocketIo(4, Taster(false, true, false, false));
                sendMessageSocketIo(3, Taster(false, true, false, false));
            });

            setInterval(() => {
                sendMessageSocketIo(-100, {next: true});
            }, 8000);
        }
        socket.on('error', err => {
            socketIo.emit('error', true);
        });
        socket.on('disconnect', err => {
            socketIo.emit('error', true);
        });
    });

    // let sendStatus = setInterval(()=>{
    //
    //  },1000);
};



